import * as React from "react";
import { ThemeProvider } from "@mui/material/styles";
import IconChevronRight from "../assets/icon/IconChevronRight"

import {
  Grid,
  Box,
  Button,
  Stack,
  Typography,
  Chip,
  Card,
  Link
} from "@mui/material";
import logoFigma from "../assets/logoFigma.svg"
import theme from "../theme";
import { NavLink } from "react-router-dom";
import { ItemCover } from "cover/ItemCover";

const TemplateList: React.FC = () => {
  return (
    <React.Fragment>
      <ThemeProvider theme={theme}>
        <Box className="templateList">
          <Typography color="text.primary" variant="h5" mb={3}>
            Templates [---------------]
          </Typography>

          <Grid container spacing={2} columns={12}>
            
            <ItemCover
              title="Nombre de Vista"
              description="Descripción de sobre la vista desarrollada"
              urlFigma=""
              stateName="inProgress"
              route="/template"
              column={6}
            />
            <ItemCover
              title="Nombre de Vista"
              description="Descripción de sobre la vista desarrollada"
              urlFigma=""
              stateName="inProgress"
              route="/template"
              column={6}
            />
            <ItemCover
              title="Nombre de Vista"
              description="Descripción de sobre la vista desarrollada"
              urlFigma=""
              stateName="inProgress"
              route="/template"
              column={6}
            />
            
          </Grid>

        </Box>
      </ThemeProvider>
    </React.Fragment>
  );
};

export default TemplateList;
