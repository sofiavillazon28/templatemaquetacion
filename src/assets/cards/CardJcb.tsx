import * as React from 'react'
import { SvgIcon as MuiSvgIcon, SvgIconProps } from '@mui/material'
import { styled, ThemeProvider } from "@mui/material/styles"

import theme from "../../theme";

const SvgIcon = styled(MuiSvgIcon, {
    shouldForwardProp: (prop) => prop !== 'fill',
  })<SvgIconProps>(({theme}) => ({
}));

const SvgIconFrame = styled(SvgIcon)(({ theme }) => ({
       fontSize: "40px !important",
      "&.MuiSvgIcon-fontSizeLarge": {
        fontSize: "51px !important"
      },
      "&.MuiSvgIcon-fontSizeSmall": {
        fontSize: "29px !important"
      },
}));

SvgIcon.defaultProps = {
    viewBox: '0 0 29 20',
    focusable: 'false',
    'aria-hidden': 'true',
};

const CardJbc: React.FunctionComponent<SvgIconProps> = (props) => {
    return (
        <ThemeProvider theme={theme}>
            <SvgIconFrame {...props} width="29" height="20" viewBox="0 0 29 20" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 2C0 0.895431 0.895431 0 2 0H27C28.1046 0 29 0.895431 29 2V18C29 19.1046 28.1046 20 27 20H2C0.89543 20 0 19.1046 0 18V2Z" fill="#F9F9F9"/>
            <path d="M20.4756 11.6771H22.1498C22.1976 11.6771 22.3093 11.661 22.3571 11.661C22.676 11.5966 22.9471 11.3067 22.9471 10.9041C22.9471 10.5176 22.676 10.2277 22.3571 10.1472C22.3093 10.1311 22.2136 10.1311 22.1498 10.1311H20.4756V11.6771Z" fill="url(#paint0_linear_131_101143)"/>
            <path d="M21.9585 1C20.364 1 19.0565 2.30444 19.0565 3.93097V6.97466H23.1543C23.25 6.97466 23.3616 6.97466 23.4413 6.99077C24.3661 7.03908 25.0518 7.5222 25.0518 8.35962C25.0518 9.0199 24.5894 9.58354 23.7283 9.69627V9.72848C24.6691 9.7929 25.3866 10.3243 25.3866 11.1456C25.3866 12.0314 24.5894 12.6111 23.537 12.6111H19.0405V18.5697H23.2978C24.8923 18.5697 26.1998 17.2652 26.1998 15.6387V1H21.9585Z" fill="url(#paint1_linear_131_101143)"/>
            <path d="M22.7398 8.55282C22.7398 8.16632 22.4687 7.90865 22.1498 7.86034C22.1179 7.86034 22.0382 7.84424 21.9904 7.84424H20.4756V9.26141H21.9904C22.0382 9.26141 22.1339 9.26141 22.1498 9.24531C22.4687 9.19699 22.7398 8.93933 22.7398 8.55282Z" fill="url(#paint2_linear_131_101143)"/>
            <path d="M5.91792 1C4.32343 1 3.01594 2.30444 3.01594 3.93097V11.1618C3.82914 11.5644 4.67422 11.822 5.5193 11.822C6.52383 11.822 7.06596 11.2101 7.06596 10.3726V6.95856H9.55337V10.3565C9.55337 11.6771 8.74018 12.7561 5.9817 12.7561C4.30748 12.7561 3 12.3857 3 12.3857V18.5536H7.2573C8.85179 18.5536 10.1593 17.2491 10.1593 15.6226V1H5.91792Z" fill="url(#paint3_linear_131_101143)"/>
            <path d="M13.9381 1C12.3436 1 11.0361 2.30444 11.0361 3.93097V7.76377C11.7696 7.1357 13.0452 6.7331 15.1021 6.82972C16.2023 6.87804 17.3822 7.18402 17.3822 7.18402V8.42404C16.7923 8.11806 16.0907 7.84429 15.1818 7.77987C13.6192 7.66714 12.6785 8.44014 12.6785 9.7929C12.6785 11.1618 13.6192 11.9348 15.1818 11.8059C16.0907 11.7415 16.7923 11.4516 17.3822 11.1618V12.4018C17.3822 12.4018 16.2182 12.7078 15.1021 12.7561C13.0452 12.8527 11.7696 12.4501 11.0361 11.822V18.5858H15.2934C16.8879 18.5858 18.1954 17.2814 18.1954 15.6548V1H13.9381Z" fill="url(#paint4_linear_131_101143)"/>
            <defs>
            <linearGradient id="paint0_linear_131_101143" x1="19.0538" y1="10.906" x2="26.2191" y2="10.906" gradientUnits="userSpaceOnUse">
            <stop stop-color="#007940"/>
            <stop offset="0.2285" stop-color="#00873F"/>
            <stop offset="0.7433" stop-color="#40A737"/>
            <stop offset="1" stop-color="#5CB531"/>
            </linearGradient>
            <linearGradient id="paint1_linear_131_101143" x1="19.0536" y1="9.77781" x2="26.2194" y2="9.77781" gradientUnits="userSpaceOnUse">
            <stop stop-color="#007940"/>
            <stop offset="0.2285" stop-color="#00873F"/>
            <stop offset="0.7433" stop-color="#40A737"/>
            <stop offset="1" stop-color="#5CB531"/>
            </linearGradient>
            <linearGradient id="paint2_linear_131_101143" x1="19.0536" y1="8.55081" x2="26.2192" y2="8.55081" gradientUnits="userSpaceOnUse">
            <stop stop-color="#007940"/>
            <stop offset="0.2285" stop-color="#00873F"/>
            <stop offset="0.7433" stop-color="#40A737"/>
            <stop offset="1" stop-color="#5CB531"/>
            </linearGradient>
            <linearGradient id="paint3_linear_131_101143" x1="3.01257" y1="9.77781" x2="10.2887" y2="9.77781" gradientUnits="userSpaceOnUse">
            <stop stop-color="#1F286F"/>
            <stop offset="0.4751" stop-color="#004E94"/>
            <stop offset="0.8261" stop-color="#0066B1"/>
            <stop offset="1" stop-color="#006FBC"/>
            </linearGradient>
            <linearGradient id="paint4_linear_131_101143" x1="10.9949" y1="9.77781" x2="18.0617" y2="9.77781" gradientUnits="userSpaceOnUse">
            <stop stop-color="#6C2C2F"/>
            <stop offset="0.1735" stop-color="#882730"/>
            <stop offset="0.5731" stop-color="#BE1833"/>
            <stop offset="0.8585" stop-color="#DC0436"/>
            <stop offset="1" stop-color="#E60039"/>
            </linearGradient>
            </defs>
            </SvgIconFrame>
        </ThemeProvider>

    );
};

export default CardJbc;