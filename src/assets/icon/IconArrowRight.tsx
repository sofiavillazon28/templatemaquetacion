import * as React from 'react'
import { SvgIcon as MuiSvgIcon, SvgIconProps, styled } from '@mui/material'
import { ThemeProvider } from "@mui/material/styles"

import theme from "../../theme";


const SvgIcon = styled(MuiSvgIcon, {
    shouldForwardProp: (prop) => prop !== 'fill',
  })<SvgIconProps>(({theme ,color,fontSize}) => ({
    fill: color + `!important`,
    fontSize: fontSize
}));

SvgIcon.defaultProps = {
    viewBox: '0 0 24 24',
    focusable: 'false',
    'aria-hidden': 'true',
};

const IconArrowRight: React.FunctionComponent<SvgIconProps> = (props) => {
    return (
        <ThemeProvider theme={theme}>
                <SvgIcon {...props} width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M3.33366 8.66671H11.2437L8.82166 11.5734C8.58566 11.856 8.62433 12.2767 8.90699 12.512C9.19033 12.748 9.61033 12.7094 9.84633 12.4267L13.1797 8.42671C13.2057 8.39537 13.2183 8.35871 13.2377 8.32404C13.2537 8.29604 13.273 8.27204 13.285 8.24137C13.315 8.16471 13.333 8.08404 13.333 8.00271C13.333 8.00204 13.3337 8.00071 13.3337 8.00004C13.3337 7.99937 13.333 7.99804 13.333 7.99737C13.333 7.91604 13.315 7.83537 13.285 7.75871C13.273 7.72804 13.2537 7.70404 13.2377 7.67604C13.2183 7.64137 13.2057 7.60471 13.1797 7.57337L9.84633 3.57337C9.71366 3.41537 9.52433 3.33337 9.33366 3.33337C9.18299 3.33337 9.03166 3.38404 8.90699 3.48804C8.62433 3.72337 8.58566 4.14404 8.82166 4.42671L11.2437 7.33337H3.33366C2.96566 7.33337 2.66699 7.63204 2.66699 8.00004C2.66699 8.36804 2.96566 8.66671 3.33366 8.66671" />
                </SvgIcon>
        </ThemeProvider>

    );
};

export default IconArrowRight;