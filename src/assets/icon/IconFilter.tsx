import * as React from 'react'
import { SvgIcon as MuiSvgIcon, SvgIconProps, styled } from '@mui/material'
import { ThemeProvider } from "@mui/material/styles"

import theme from "../../theme";


const SvgIcon = styled(MuiSvgIcon, {
    name: 'MopeimIcon',
    shouldForwardProp: (prop) => prop !== 'fill',
  })<SvgIconProps>(({theme ,color,fontSize}) => ({
    fill: color + `!important`,
    fontSize: fontSize
}));

SvgIcon.defaultProps = {
    viewBox: '0 0 24 24',
    focusable: 'false',
    'aria-hidden': 'true',
};

const Iconfilter: React.FunctionComponent<SvgIconProps> = (props) => {
    return (
        <ThemeProvider theme={theme}>
                <SvgIcon {...props} width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M2.06459 2.39182C2.17394 2.15236 2.40452 2 2.65754 2H13.3425C13.5955 2 13.8261 2.15236 13.9354 2.39182C14.0448 2.63127 14.0135 2.91533 13.855 3.12231L9.72603 8.51431V13.3099C9.72603 13.5516 9.60553 13.7757 9.40837 13.9007C9.21121 14.0256 8.96614 14.0332 8.76232 13.9206L6.62534 12.7406C6.40926 12.6213 6.27397 12.3862 6.27397 12.13V8.51431L2.145 3.12231C1.9865 2.91533 1.95523 2.63127 2.06459 2.39182ZM4.0295 3.3801L7.44405 7.83914C7.53789 7.96168 7.58904 8.11418 7.58904 8.2714V11.7133L8.41096 12.1671V8.2714C8.41096 8.11418 8.46211 7.96168 8.55595 7.83914L11.9705 3.3801H4.0295Z" />
                </SvgIcon>

        </ThemeProvider>

    );
};

export default Iconfilter;