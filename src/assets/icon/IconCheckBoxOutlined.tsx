import * as React from 'react'
import { SvgIcon as MuiSvgIcon, SvgIconProps, styled } from '@mui/material'
import { ThemeProvider } from "@mui/material/styles"

import theme from "../../theme";


const SvgIcon = styled(MuiSvgIcon, {
    name: 'MopeimIcon',
    shouldForwardProp: (prop) => prop !== 'fill',
  })<SvgIconProps>(({theme ,color,fontSize}) => ({
    fill: color + `!important`,
    fontSize: fontSize
}));

SvgIcon.defaultProps = {
    viewBox: '0 0 24 24',
    focusable: 'false',
    'aria-hidden': 'true',
};

const IconCheckBoxOutlined: React.FunctionComponent<SvgIconProps> = (props) => {
    return (
        <ThemeProvider theme={theme}>
            
                <SvgIcon {...props} width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M14.6667 1H1.33333C1.14924 1 1 1.14924 1 1.33333V14.6667C1 14.8508 1.14924 15 1.33333 15H14.6667C14.8508 15 15 14.8508 15 14.6667V1.33333C15 1.14924 14.8508 1 14.6667 1ZM1.33333 0C0.596954 0 0 0.596953 0 1.33333V14.6667C0 15.403 0.596953 16 1.33333 16H14.6667C15.403 16 16 15.403 16 14.6667V1.33333C16 0.596954 15.403 0 14.6667 0H1.33333Z"/>
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M12.0202 5.86861C12.2155 6.06388 12.2155 6.38046 12.0202 6.57572L7.57578 11.0202C7.47935 11.1166 7.34771 11.1695 7.21136 11.1665C7.07502 11.1635 6.9458 11.105 6.85365 11.0045L4.63143 8.58023C4.44483 8.37667 4.45858 8.06039 4.66214 7.87379C4.8657 7.6872 5.18198 7.70095 5.36858 7.90451L7.23793 9.9438L11.3131 5.86861C11.5084 5.67335 11.825 5.67335 12.0202 5.86861Z"/>  
                </SvgIcon>

        </ThemeProvider>

    );
};

export default IconCheckBoxOutlined;