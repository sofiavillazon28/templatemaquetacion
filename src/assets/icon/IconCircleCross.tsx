import * as React from 'react'
import { SvgIcon as MuiSvgIcon, SvgIconProps, styled } from '@mui/material'
import { ThemeProvider } from "@mui/material/styles"

import theme from "../../theme";


const SvgIcon = styled(MuiSvgIcon, {
    name: 'MopeimIcon',
    shouldForwardProp: (prop) => prop !== 'fill',
  })<SvgIconProps>(({color,fontSize}) => ({
    fill: color + `!important`,
    fontSize: fontSize
}));

SvgIcon.defaultProps = {
    viewBox: '0 0 24 24',
    focusable: 'false',
    'aria-hidden': 'true',
};

const IconCircleCross: React.FunctionComponent<SvgIconProps> = (props) => {
    return (
        <ThemeProvider theme={theme}>
            <SvgIcon {...props}  width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M6.11194 10.5532C6.0242 10.5537 5.93723 10.5369 5.856 10.5038C5.77478 10.4706 5.7009 10.4217 5.63861 10.3599C5.57612 10.2979 5.52652 10.2242 5.49268 10.143C5.45883 10.0617 5.44141 9.97458 5.44141 9.88658C5.44141 9.79857 5.45883 9.71143 5.49268 9.63019C5.52652 9.54895 5.57612 9.47522 5.63861 9.41324L9.41194 5.63991C9.53747 5.51437 9.70774 5.44385 9.88527 5.44385C10.0628 5.44385 10.2331 5.51437 10.3586 5.63991C10.4841 5.76544 10.5547 5.93571 10.5547 6.11324C10.5547 6.29078 10.4841 6.46104 10.3586 6.58658L6.58527 10.3599C6.52298 10.4217 6.4491 10.4706 6.36788 10.5038C6.28665 10.5369 6.19968 10.5537 6.11194 10.5532Z"/>
                <path d="M9.88609 10.5532C9.79835 10.5537 9.71137 10.5369 9.63015 10.5038C9.54892 10.4706 9.47505 10.4217 9.41275 10.3599L5.63942 6.58658C5.51388 6.46104 5.44336 6.29078 5.44336 6.11324C5.44336 5.93571 5.51388 5.76544 5.63942 5.63991C5.76496 5.51437 5.93522 5.44385 6.11275 5.44385C6.29029 5.44385 6.46055 5.51437 6.58609 5.63991L10.3594 9.41324C10.4219 9.47522 10.4715 9.54895 10.5053 9.63019C10.5392 9.71143 10.5566 9.79857 10.5566 9.88658C10.5566 9.97458 10.5392 10.0617 10.5053 10.143C10.4715 10.2242 10.4219 10.2979 10.3594 10.3599C10.2971 10.4217 10.2233 10.4706 10.142 10.5038C10.0608 10.5369 9.97383 10.5537 9.88609 10.5532Z"/>
            </SvgIcon>
        </ThemeProvider>
    );
};

export default IconCircleCross;
