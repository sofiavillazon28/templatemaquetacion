import * as React from 'react'
import { SvgIcon as MuiSvgIcon, SvgIconProps, styled } from '@mui/material'
import { ThemeProvider } from "@mui/material/styles"

import theme from "../../theme";


const SvgIcon = styled(MuiSvgIcon, {
    name: 'MopeimIcon',
    shouldForwardProp: (prop) => prop !== 'fill',
  })<SvgIconProps>(({color,fontSize}) => ({
    fill: color + `!important`,
    fontSize: fontSize
}));

SvgIcon.defaultProps = {
    viewBox: '0 0 24 24',
    focusable: 'false',
    'aria-hidden': 'true',
};

const IconNavAlt: React.FunctionComponent<SvgIconProps> = (props) => {
    return (
        <ThemeProvider theme={theme}>
            
            <SvgIcon {...props} width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M13.4271 4.72085H2.76042C2.58361 4.72085 2.41404 4.65061 2.28901 4.52558C2.16399 4.40056 2.09375 4.23099 2.09375 4.05418C2.09375 3.87737 2.16399 3.7078 2.28901 3.58277C2.41404 3.45775 2.58361 3.38751 2.76042 3.38751H13.4271C13.6039 3.38751 13.7735 3.45775 13.8985 3.58277C14.0235 3.7078 14.0938 3.87737 14.0938 4.05418C14.0938 4.23099 14.0235 4.40056 13.8985 4.52558C13.7735 4.65061 13.6039 4.72085 13.4271 4.72085Z" />
                <path d="M8.09375 9.38747H2.76042C2.58361 9.38747 2.41404 9.31723 2.28901 9.19221C2.16399 9.06718 2.09375 8.89762 2.09375 8.7208C2.09375 8.54399 2.16399 8.37442 2.28901 8.2494C2.41404 8.12438 2.58361 8.05414 2.76042 8.05414H8.09375C8.27056 8.05414 8.44013 8.12438 8.56515 8.2494C8.69018 8.37442 8.76042 8.54399 8.76042 8.7208C8.76042 8.89762 8.69018 9.06718 8.56515 9.19221C8.44013 9.31723 8.27056 9.38747 8.09375 9.38747Z" />
                <path d="M13.4271 14.0542H2.76042C2.58361 14.0542 2.41404 13.9839 2.28901 13.8589C2.16399 13.7339 2.09375 13.5643 2.09375 13.3875C2.09375 13.2107 2.16399 13.0411 2.28901 12.9161C2.41404 12.7911 2.58361 12.7208 2.76042 12.7208H13.4271C13.6039 12.7208 13.7735 12.7911 13.8985 12.9161C14.0235 13.0411 14.0938 13.2107 14.0938 13.3875C14.0938 13.5643 14.0235 13.7339 13.8985 13.8589C13.7735 13.9839 13.6039 14.0542 13.4271 14.0542Z" />
            </SvgIcon>
            
        </ThemeProvider>

    );
};

export default IconNavAlt;