import * as React from 'react'
import { SvgIcon as MuiSvgIcon, SvgIconProps, styled } from '@mui/material'
import { ThemeProvider } from "@mui/material/styles"

import theme from "../../theme";


const SvgIcon = styled(MuiSvgIcon, {
    name: 'MopeimIcon',
    shouldForwardProp: (prop) => prop !== 'fill',
  })<SvgIconProps>(({theme ,color,fontSize}) => ({
    fill: color + `!important`,
    fontSize: fontSize
}));

SvgIcon.defaultProps = {
    viewBox: '0 0 24 24',
    focusable: 'false',
    'aria-hidden': 'true',
};

const IconChevronLeft: React.FunctionComponent<SvgIconProps> = (props) => {
    return (
        <ThemeProvider theme={theme}>
                <SvgIcon {...props} width="17" height="17" viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M10.9991 14.3733C10.8316 14.3746 10.6698 14.3127 10.5458 14.2L4.54581 8.66666C4.48119 8.60682 4.42907 8.53477 4.39245 8.45467C4.35583 8.37457 4.33545 8.28801 4.33248 8.19999C4.32608 8.02561 4.38831 7.85567 4.50581 7.72666L9.93248 1.83999C9.99177 1.77482 10.0634 1.72204 10.1432 1.68472C10.223 1.64739 10.3094 1.62626 10.3975 1.62254C10.4855 1.61882 10.5734 1.63259 10.6561 1.66305C10.7387 1.69351 10.8146 1.74005 10.8791 1.79999C11.0078 1.92044 11.0836 2.08695 11.0898 2.26311C11.0961 2.43927 11.0323 2.61073 10.9125 2.73999L5.93915 8.13332L11.4525 13.22C11.5821 13.3395 11.6591 13.5056 11.6666 13.6817C11.6741 13.8579 11.6115 14.0299 11.4925 14.16C11.4296 14.2278 11.3532 14.2818 11.2683 14.3186C11.1833 14.3553 11.0917 14.3739 10.9991 14.3733Z" />
                </SvgIcon>

        </ThemeProvider>

    );
};

export default IconChevronLeft;