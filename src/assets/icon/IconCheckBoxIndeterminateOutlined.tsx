import * as React from 'react'
import { SvgIcon as MuiSvgIcon, SvgIconProps, styled } from '@mui/material'
import { ThemeProvider } from "@mui/material/styles"

import theme from "../../theme";


const SvgIcon = styled(MuiSvgIcon, {
    name: 'MopeimIcon',
    shouldForwardProp: (prop) => prop !== 'fill',
  })<SvgIconProps>(({theme ,color,fontSize}) => ({
    fill: color + `!important`,
    fontSize: fontSize
}));

SvgIcon.defaultProps = {
    viewBox: '0 0 24 24',
    focusable: 'false',
    'aria-hidden': 'true',
};

const IconCheckBoxIndeterminateOutlined: React.FunctionComponent<SvgIconProps> = (props) => {
    return (
        <ThemeProvider theme={theme}>
            
                <SvgIcon {...props} width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M14.6667 1H1.33333C1.14924 1 1 1.14924 1 1.33333V14.6667C1 14.8508 1.14924 15 1.33333 15H14.6667C14.8508 15 15 14.8508 15 14.6667V1.33333C15 1.14924 14.8508 1 14.6667 1ZM1.33333 0C0.596954 0 0 0.596953 0 1.33333V14.6667C0 15.403 0.596953 16 1.33333 16H14.6667C15.403 16 16 15.403 16 14.6667V1.33333C16 0.596954 15.403 0 14.6667 0H1.33333Z"/>
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M3.94434 8C3.94434 7.72386 4.16819 7.5 4.44434 7.5H11.5554C11.8316 7.5 12.0554 7.72386 12.0554 8C12.0554 8.27614 11.8316 8.5 11.5554 8.5H4.44434C4.16819 8.5 3.94434 8.27614 3.94434 8Z"/>
                </SvgIcon>

        </ThemeProvider>

    );
};

export default IconCheckBoxIndeterminateOutlined;