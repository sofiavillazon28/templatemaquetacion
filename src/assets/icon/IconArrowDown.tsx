import * as React from 'react'
import { SvgIcon as MuiSvgIcon, SvgIconProps, styled } from '@mui/material'
import { ThemeProvider } from "@mui/material/styles"

import theme from "../../theme";


const SvgIcon = styled(MuiSvgIcon, {
    name: 'MopeimIcon',
    shouldForwardProp: (prop) => prop !== 'fill',
  })<SvgIconProps>(({theme ,color,fontSize}) => ({
    fill: color + `!important`,
    fontSize: fontSize
}));

SvgIcon.defaultProps = {
    viewBox: '0 0 24 24',
    focusable: 'false',
    'aria-hidden': 'true',
};

const IconArrowDown: React.FunctionComponent<SvgIconProps> = (props) => {
    return (
        <ThemeProvider theme={theme}>
            
                <SvgIcon {...props} width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6.94005 10.5533C6.85041 10.5503 6.76229 10.5293 6.68096 10.4915C6.59962 10.4537 6.52676 10.3999 6.46671 10.3333L0.813381 4.69996C0.704163 4.57243 0.647093 4.40838 0.653573 4.24059C0.660054 4.07281 0.729609 3.91365 0.848339 3.79492C0.967068 3.67619 1.12623 3.60664 1.29401 3.60015C1.4618 3.59367 1.62585 3.65074 1.75338 3.75996L6.94005 8.94663L12.2734 3.6133C12.3983 3.48913 12.5673 3.41943 12.7434 3.41943C12.9195 3.41943 13.0885 3.48913 13.2134 3.6133C13.2759 3.67527 13.3255 3.749 13.3593 3.83024C13.3932 3.91148 13.4106 3.99862 13.4106 4.08663C13.4106 4.17464 13.3932 4.26177 13.3593 4.34301C13.3255 4.42425 13.2759 4.49799 13.2134 4.55996L7.41338 10.3333C7.35334 10.3999 7.28047 10.4537 7.19914 10.4915C7.11781 10.5293 7.02969 10.5503 6.94005 10.5533Z" />
                </SvgIcon>
        </ThemeProvider>

    );
};

export default IconArrowDown;