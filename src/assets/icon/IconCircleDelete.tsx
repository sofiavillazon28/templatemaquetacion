import * as React from 'react'
import { SvgIcon as MuiSvgIcon, SvgIconProps, styled } from '@mui/material'
import { ThemeProvider } from "@mui/material/styles"

import theme from "../../theme";


const SvgIcon = styled(MuiSvgIcon, {
    name: 'MopeimIcon',
    shouldForwardProp: (prop) => prop !== 'fill',
  })<SvgIconProps>(({theme ,color,fontSize}) => ({
    fill: color + `!important`,
    fontSize: fontSize
}));

SvgIcon.defaultProps = {
    viewBox: '0 0 24 24',
    focusable: 'false',
    'aria-hidden': 'true',
};

const IconCircleDelete: React.FunctionComponent<SvgIconProps> = (props) => {
    return (
        <ThemeProvider theme={theme}>
                <SvgIcon {...props}  width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M10.668 8.66671H5.33464C5.15782 8.66671 4.98826 8.59647 4.86323 8.47144C4.73821 8.34642 4.66797 8.17685 4.66797 8.00004C4.66797 7.82323 4.73821 7.65366 4.86323 7.52864C4.98826 7.40361 5.15782 7.33337 5.33464 7.33337H10.668C10.8448 7.33337 11.0143 7.40361 11.1394 7.52864C11.2644 7.65366 11.3346 7.82323 11.3346 8.00004C11.3346 8.17685 11.2644 8.34642 11.1394 8.47144C11.0143 8.59647 10.8448 8.66671 10.668 8.66671Z"/>
                </SvgIcon>

        </ThemeProvider>

    );
};

export default IconCircleDelete;