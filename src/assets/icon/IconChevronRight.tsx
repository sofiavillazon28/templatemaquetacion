import * as React from 'react'
import { SvgIcon as MuiSvgIcon, SvgIconProps, styled } from '@mui/material'
import { ThemeProvider } from "@mui/material/styles"

import theme from "../../theme";


const SvgIcon = styled(MuiSvgIcon, {
    name: 'MopeimIcon',
    shouldForwardProp: (prop) => prop !== 'fill',
  })<SvgIconProps>(({theme ,color,fontSize}) => ({
    fill: color + `!important`,
    fontSize: fontSize
}));

SvgIcon.defaultProps = {
    viewBox: '0 0 24 24',
    focusable: 'false',
    'aria-hidden': 'true',
};

const IconChevronRight: React.FunctionComponent<SvgIconProps> = (props) => {
    return (
        <ThemeProvider theme={theme}>
                <SvgIcon {...props} width="17" height="17" viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M5.23289 14.3801C5.14516 14.3806 5.05818 14.3638 4.97696 14.3306C4.89573 14.2974 4.82185 14.2485 4.75956 14.1867C4.63539 14.0618 4.5657 13.8929 4.5657 13.7167C4.5657 13.5406 4.63539 13.3717 4.75956 13.2467L9.94623 8.06008L4.61289 2.72674C4.5036 2.59643 4.44825 2.42927 4.45817 2.25948C4.46808 2.0897 4.54251 1.93012 4.66623 1.81341C4.7282 1.75092 4.80194 1.70133 4.88318 1.66748C4.96442 1.63364 5.05155 1.61621 5.13956 1.61621C5.22757 1.61621 5.31471 1.63364 5.39595 1.66748C5.47718 1.70133 5.55092 1.75092 5.61289 1.81341L11.3329 7.58674C11.3954 7.64872 11.445 7.72245 11.4788 7.80369C11.5127 7.88493 11.5301 7.97207 11.5301 8.06008C11.5301 8.14808 11.5127 8.23522 11.4788 8.31646C11.445 8.3977 11.3954 8.47143 11.3329 8.53341L5.69956 14.1867C5.57539 14.3099 5.40779 14.3793 5.23289 14.3801Z"/>
                </SvgIcon>

        </ThemeProvider>

    );
};

export default IconChevronRight;