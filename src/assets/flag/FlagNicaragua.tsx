import * as React from 'react'
import { SvgIcon as MuiSvgIcon, SvgIconProps, styled } from '@mui/material'
import { ThemeProvider } from "@mui/material/styles"

import theme from "../../theme";

const SvgIcon = styled(MuiSvgIcon, {
    shouldForwardProp: (prop) => prop !== 'fill',
  })<SvgIconProps>(({theme ,fontSize}) => ({
    width: fontSize
}));

SvgIcon.defaultProps = {
    viewBox: '0 0 24 24',
    focusable: 'false',
    'aria-hidden': 'true',
};

const FlagNicaragua: React.FunctionComponent<SvgIconProps> = (props) => {
    return (
        <ThemeProvider theme={theme}>
            <SvgIcon {...props}  width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M12 24C18.6274 24 24 18.6274 24 12C24 5.37258 18.6274 0 12 0C5.37258 0 0 5.37258 0 12C0 18.6274 5.37258 24 12 24Z" fill="#F0F0F0"/>
                <path d="M11.9999 0C7.24242 0 3.13167 2.76867 1.19067 6.78263H22.8092C20.8682 2.76867 16.7574 0 11.9999 0Z" fill="#338AF3"/>
                <path d="M12 23.9999C16.7574 23.9999 20.8682 21.2312 22.8092 17.2173H1.19067C3.13167 21.2312 7.24242 23.9999 12 23.9999Z" fill="#338AF3"/>
                <path d="M12 8.3479C9.98292 8.3479 8.34778 9.98304 8.34778 12.0001C8.34778 14.0171 9.98292 15.6522 12 15.6522C14.017 15.6522 15.6521 14.0171 15.6521 12.0001C15.6521 9.98304 14.017 8.3479 12 8.3479ZM12 14.087C10.8474 14.087 9.91298 13.1527 9.91298 12.0001C9.91298 10.8474 10.8473 9.9131 12 9.9131C13.1526 9.9131 14.0869 10.8474 14.0869 12.0001C14.0869 13.1527 13.1525 14.087 12 14.087Z" fill="#FFDA44"/>
                <path d="M13.8075 12.5217L12.0001 12L10.1926 12.5217L9.59021 13.5652H14.4099L13.8075 12.5217Z" fill="#0052B4"/>
                <path d="M12 9.39136L10.795 11.4783L12 12L13.2049 11.4783L12 9.39136Z" fill="#338AF3"/>
                <path d="M10.1926 12.5217H13.8074L13.205 11.4783H10.7951L10.1926 12.5217Z" fill="#6DA544"/>
            </SvgIcon>


        </ThemeProvider>

    );
};

export default FlagNicaragua;