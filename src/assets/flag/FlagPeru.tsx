import * as React from 'react'
import { SvgIcon as MuiSvgIcon, SvgIconProps, styled } from '@mui/material'
import { ThemeProvider } from "@mui/material/styles"

import theme from "../../theme";

const SvgIcon = styled(MuiSvgIcon, {
    shouldForwardProp: (prop) => prop !== 'fill',
  })<SvgIconProps>(({theme ,fontSize}) => ({
    width: fontSize
}));

SvgIcon.defaultProps = {
    viewBox: '0 0 24 24',
    focusable: 'false',
    'aria-hidden': 'true',
};

const FlagPeru: React.FunctionComponent<SvgIconProps> = (props) => {
    return (
        <ThemeProvider theme={theme}>
            <SvgIcon {...props} width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <g clip-path="url(#clip0_100_104015)">
                <path d="M0 11.9767C0 17.096 3.2 21.4639 7.76471 23.2017V0.798584C3.2 2.48939 0 6.85729 0 11.9767Z" fill="#D80027"/>
                <path d="M24.0001 11.9765C24.0001 6.85714 20.8001 2.48923 16.2354 0.751465V23.1546C20.8001 21.4638 24.0001 17.0959 24.0001 11.9765Z" fill="#D80027"/>
                <path d="M7.76465 0.798434V23.2016C9.08229 23.7182 10.5411 24 12.047 24C13.5529 24 14.9646 23.7182 16.3294 23.2016V0.798434C14.9646 0.2818 13.5058 0 11.9999 0C10.4941 0 9.08229 0.2818 7.76465 0.798434Z" fill="white"/>
                </g>
                <defs>
                <clipPath id="clip0_100_104015">
                <rect width="24" height="24" fill="white"/>
                </clipPath>
                </defs>
            </SvgIcon>
        </ThemeProvider>

    );
};

export default FlagPeru;