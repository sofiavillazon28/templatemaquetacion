import * as React from 'react'
import { SvgIcon as MuiSvgIcon, SvgIconProps, styled } from '@mui/material'
import { ThemeProvider } from "@mui/material/styles"

import theme from "../../theme";

const SvgIcon = styled(MuiSvgIcon, {
    shouldForwardProp: (prop) => prop !== 'fill',
  })<SvgIconProps>(({theme ,fontSize}) => ({
    width: fontSize
}));

SvgIcon.defaultProps = {
    viewBox: '0 0 24 24',
    focusable: 'false',
    'aria-hidden': 'true',
};

const FlagEcuador: React.FunctionComponent<SvgIconProps> = (props) => {
    return (
        <ThemeProvider theme={theme}>
            <SvgIcon {...props} width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M0 12C0 5.37262 5.32096 0 11.8846 0C18.4483 0 23.7692 5.37262 23.7692 12L11.8846 13.0435L0 12Z" fill="#FFDA44"/>
                <path d="M1.59033 17.9999C3.6453 21.5866 7.4857 23.9999 11.8846 23.9999C16.2836 23.9999 20.124 21.5866 22.1789 17.9999L11.8846 17.2173L1.59033 17.9999Z" fill="#D80027"/>
                <path d="M22.1789 18C23.1901 16.235 23.7692 14.1858 23.7692 12H0C0 14.1858 0.579096 16.235 1.59031 18H22.1789Z" fill="#0052B4"/>
                <path d="M11.8846 16.174C14.1676 16.174 16.0184 14.3052 16.0184 12.0001C16.0184 9.69489 14.1676 7.82617 11.8846 7.82617C9.6016 7.82617 7.75085 9.69489 7.75085 12.0001C7.75085 14.3052 9.6016 16.174 11.8846 16.174Z" fill="#FFDA44"/>
                <path d="M11.8847 14.6087C10.4601 14.6087 9.30115 13.4385 9.30115 12.0001V10.4349C9.30115 8.99641 10.4602 7.82617 11.8847 7.82617C13.3093 7.82617 14.4684 8.99645 14.4684 10.4349V12.0001C14.4684 13.4385 13.3094 14.6087 11.8847 14.6087Z" fill="#338AF3"/>
                <path d="M16.0181 5.73928H12.9177C12.9177 5.163 12.455 4.6958 11.8843 4.6958C11.3136 4.6958 10.8508 5.163 10.8508 5.73928H7.75049C7.75049 6.31561 8.24765 6.78277 8.81834 6.78277H8.78394C8.78394 7.3591 9.2466 7.82625 9.81739 7.82625C9.81739 8.40258 10.2801 8.86974 10.8508 8.86974H12.9177C13.4885 8.86974 13.9512 8.40258 13.9512 7.82625C14.522 7.82625 14.9846 7.3591 14.9846 6.78277H14.9502C15.521 6.78277 16.0181 6.31557 16.0181 5.73928Z" fill="black"/>
            </SvgIcon>

        </ThemeProvider>

    );
};

export default FlagEcuador;