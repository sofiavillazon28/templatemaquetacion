import * as React from 'react'
import { SvgIcon as MuiSvgIcon, SvgIconProps, styled } from '@mui/material'
import { ThemeProvider } from "@mui/material/styles"

import theme from "../../theme";

const SvgIcon = styled(MuiSvgIcon, {
    shouldForwardProp: (prop) => prop !== 'fill',
  })<SvgIconProps>(({theme ,fontSize}) => ({
    width: fontSize
}));

SvgIcon.defaultProps = {
    viewBox: '0 0 24 24',
    focusable: 'false',
    'aria-hidden': 'true',
};

const FlagColombia: React.FunctionComponent<SvgIconProps> = (props) => {
    return (
        <ThemeProvider theme={theme}>
            <SvgIcon {...props} width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <g clip-path="url(#clip0_100_104023)">
                <path d="M12 24C18.6274 24 24 18.6274 24 12C24 5.37258 18.6274 0 12 0C5.37258 0 0 5.37258 0 12C0 18.6274 5.37258 24 12 24Z" fill="url(#paint0_radial_100_104023)"/>
                <path d="M0 12C0 14.1176 0.564706 16.1412 1.50588 17.8824H22.4941C23.4823 16.1412 24 14.1176 24 12C24 11.9529 24 11.9059 24 11.8118H0C0 11.9059 0 11.9529 0 12Z" fill="#003893"/>
                <path d="M24 11.8587C23.9529 9.64697 23.3412 7.62344 22.3059 5.83521H1.69412C0.658823 7.57638 0.0470588 9.64697 0 11.8587H24Z" fill="#F3D02F"/>
                <path d="M12 23.9999C16.5176 23.9999 20.4235 21.5058 22.4941 17.8352H1.50586C3.57645 21.5529 7.48233 23.9999 12 23.9999Z" fill="#CE1126"/>
                <path d="M11.9999 0C7.62338 0 3.81162 2.35294 1.69397 5.8353H22.2587C20.1881 2.35294 16.3763 0 11.9999 0Z" fill="#F3D02F"/>
                </g>
                <defs>
                <radialGradient id="paint0_radial_100_104023" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(8.90936 6.73463) scale(13.5515)">
                <stop stop-color="#F2F2F2"/>
                <stop offset="1" stop-color="#D6D6D6"/>
                </radialGradient>
                <clipPath id="clip0_100_104023">
                <rect width="24" height="24" fill="white"/>
                </clipPath>
                </defs>
            </SvgIcon>
        </ThemeProvider>

    );
};

export default FlagColombia;