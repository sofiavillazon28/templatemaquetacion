import * as React from 'react'
import { SvgIcon as MuiSvgIcon, SvgIconProps, styled } from '@mui/material'
import { ThemeProvider } from "@mui/material/styles"

import theme from "../../theme";

const SvgIcon = styled(MuiSvgIcon, {
    shouldForwardProp: (prop) => prop !== 'fill',
  })<SvgIconProps>(({theme ,fontSize}) => ({
    width: fontSize
}));

SvgIcon.defaultProps = {
    viewBox: '0 0 24 24',
    focusable: 'false',
    'aria-hidden': 'true',
};

const FlagBrasil: React.FunctionComponent<SvgIconProps> = (props) => {
    return (
        <ThemeProvider theme={theme}>
            <SvgIcon {...props} width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M12 24C18.6274 24 24 18.6274 24 12C24 5.37258 18.6274 0 12 0C5.37258 0 0 5.37258 0 12C0 18.6274 5.37258 24 12 24Z" fill="#6DA544"/>
                <path d="M11.9999 4.69556L21.913 11.9999L11.9999 19.3042L2.08691 11.9999L11.9999 4.69556Z" fill="#FFDA44"/>
                <path d="M12.0001 16.1737C14.3052 16.1737 16.174 14.305 16.174 11.9998C16.174 9.69464 14.3052 7.82593 12.0001 7.82593C9.69489 7.82593 7.82617 9.69464 7.82617 11.9998C7.82617 14.305 9.69489 16.1737 12.0001 16.1737Z" fill="#F0F0F0"/>
                <path d="M9.91312 11.739C9.18731 11.739 8.48686 11.8494 7.82751 12.0542C7.85672 14.3342 9.71316 16.1738 12.0001 16.1738C13.4142 16.1738 14.6632 15.4699 15.4182 14.3941C14.1264 12.777 12.1389 11.739 9.91312 11.739Z" fill="#0052B4"/>
                <path d="M16.0966 12.7995C16.1469 12.5406 16.174 12.2734 16.174 11.9998C16.174 9.6946 14.3053 7.82593 12.0001 7.82593C10.2801 7.82593 8.80355 8.8666 8.16455 10.3524C8.72939 10.2354 9.31421 10.1738 9.91322 10.1738C12.3375 10.1737 14.5305 11.1814 16.0966 12.7995Z" fill="#0052B4"/>
            </SvgIcon>
        </ThemeProvider>

    );
};

export default FlagBrasil;