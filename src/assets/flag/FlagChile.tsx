import * as React from 'react'
import { SvgIcon as MuiSvgIcon, SvgIconProps, styled } from '@mui/material'
import { ThemeProvider } from "@mui/material/styles"

import theme from "../../theme";

const SvgIcon = styled(MuiSvgIcon, {
    shouldForwardProp: (prop) => prop !== 'fill',
  })<SvgIconProps>(({theme ,fontSize}) => ({
    width: fontSize
}));

SvgIcon.defaultProps = {
    viewBox: '0 0 24 24',
    focusable: 'false',
    'aria-hidden': 'true',
};

const FlagChile: React.FunctionComponent<SvgIconProps> = (props) => {
    return (
        <ThemeProvider theme={theme}>
            <SvgIcon {...props} width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M23.9999 12C23.9999 5.6 19.0116 0.376471 12.6587 0V12H23.9999Z" fill="white"/>
                <path d="M0 12H12.6588V0C12.4235 0 12.2353 0 12 0C5.36471 0 0 5.36471 0 12ZM9.22353 10.0235L7.34118 8.70588L5.45882 10.0235L6.16471 7.90588L4.28235 6.58824H6.58824L7.29412 4.47059L8 6.58824H10.3529L8.47059 7.90588L9.22353 10.0235Z" fill="#2530A9"/>
                <path d="M12.6587 24C19.0116 23.6706 23.9999 18.4 23.9999 12H12.6587V24Z" fill="#C22623"/>
                <path d="M12 24C12.2353 24 12.4706 24 12.6588 24V12H0C0 18.6353 5.36471 24 12 24Z" fill="#C22623"/>
                <path d="M8.04724 6.58817L7.34135 4.51758L6.63547 6.58817H4.32959L6.21194 7.90581L5.50606 10.0235L7.34135 8.70581L9.22371 10.0235L8.51782 7.90581L10.3531 6.58817H8.04724Z" fill="white"/>
            </SvgIcon>
        </ThemeProvider>

    );
};

export default FlagChile;