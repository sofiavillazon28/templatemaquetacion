import * as React from 'react'
import { SvgIcon as MuiSvgIcon, SvgIconProps, styled } from '@mui/material'
import { ThemeProvider } from "@mui/material/styles"

import theme from "../../theme";


const SvgIcon = styled(MuiSvgIcon, {
    shouldForwardProp: (prop) => prop !== 'fill',
  })<SvgIconProps>(({theme ,color,fontSize}) => ({
    fill: color + `!important`,
    width: fontSize
}));

SvgIcon.defaultProps = {
    viewBox: '0 0 24 24',
    focusable: 'false',
    'aria-hidden': 'true',
};

const FlagMexico: React.FunctionComponent<SvgIconProps> = (props) => {
    return (
        <ThemeProvider theme={theme}>

            <SvgIcon {...props}  width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M12 24C18.6274 24 24 18.6274 24 12C24 5.37258 18.6274 0 12 0C5.37258 0 0 5.37258 0 12C0 18.6274 5.37258 24 12 24Z" fill="white"/>
                <path d="M24 11.9999C24 7.24242 21.2314 3.13167 17.2174 1.19067V22.8092C21.2314 20.8682 24 16.7575 24 11.9999Z" fill="#D80027"/>
                <path d="M0 12C0 16.7575 2.76867 20.8682 6.78262 22.8092V1.19067C2.76867 3.13167 0 7.24242 0 12Z" fill="#11865D"/>
                <path d="M8.86938 12C8.86938 13.7289 10.2709 15.1305 11.9998 15.1305C13.7287 15.1305 15.1303 13.7289 15.1303 12V10.9565H8.86938V12Z" fill="#11865D"/>
                <path d="M16.1737 9.91311H13.0432C13.0432 9.33683 12.576 8.86963 11.9997 8.86963C11.4235 8.86963 10.9563 9.33683 10.9563 9.91311H7.82581C7.82581 10.4894 8.32779 10.9566 8.90402 10.9566H8.86929C8.86929 11.5329 9.33645 12.0001 9.91277 12.0001C9.91277 12.5764 10.3799 13.0436 10.9563 13.0436H13.0432C13.6196 13.0436 14.0867 12.5764 14.0867 12.0001C14.663 12.0001 15.1302 11.5329 15.1302 10.9566H15.0954C15.6717 10.9566 16.1737 10.4894 16.1737 9.91311Z" fill="#FF9811"/>
            </SvgIcon>

        </ThemeProvider>

    );
};

export default FlagMexico;