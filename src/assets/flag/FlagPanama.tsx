import * as React from 'react'
import { SvgIcon as MuiSvgIcon, SvgIconProps, styled } from '@mui/material'
import { ThemeProvider } from "@mui/material/styles"

import theme from "../../theme";

const SvgIcon = styled(MuiSvgIcon, {
    shouldForwardProp: (prop) => prop !== 'fill',
  })<SvgIconProps>(({theme ,fontSize}) => ({
    width: fontSize
}));

SvgIcon.defaultProps = {
    viewBox: '0 0 24 24',
    focusable: 'false',
    'aria-hidden': 'true',
};

const FlagPanama: React.FunctionComponent<SvgIconProps> = (props) => {
    return (
        <ThemeProvider theme={theme}>
            <SvgIcon {...props} width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <g clip-path="url(#clip0_100_104017)">
                <path d="M11.8846 24C18.4483 24 23.7692 18.6274 23.7692 12C23.7692 5.37258 18.4483 0 11.8846 0C5.32092 0 0 5.37258 0 12C0 18.6274 5.32092 24 11.8846 24Z" fill="#F0F0F0"/>
                <path d="M0 12C0 18.6274 5.32096 24 11.8846 24C11.8846 19.4365 11.8846 12 11.8846 12C11.8846 12 4.13376 12 0 12Z" fill="#0052B4"/>
                <path d="M11.8846 0C18.4483 0 23.7693 5.37262 23.7693 12C19.2496 12 11.8846 12 11.8846 12C11.8846 12 11.8846 4.17389 11.8846 0Z" fill="#D80027"/>
                <path d="M7.07448 4.17383L7.84406 6.5653H10.3344L8.31967 8.04327L9.0892 10.4347L7.07448 8.95672L5.05977 10.4347L5.82929 8.04327L3.81458 6.5653H6.30491L7.07448 4.17383Z" fill="#0052B4"/>
                <path d="M16.6947 13.5652L17.4643 15.9567H19.9546L17.9399 17.4346L18.7094 19.8261L16.6947 18.3481L14.68 19.8261L15.4495 17.4346L13.4348 15.9567H15.9252L16.6947 13.5652Z" fill="#D80027"/>
                </g>
                <defs>
                <clipPath id="clip0_100_104017">
                <rect width="23.7692" height="24" fill="white"/>
                </clipPath>
                </defs>
            </SvgIcon>
        </ThemeProvider>

    );
};

export default FlagPanama;