import * as React from "react";

import { useState } from "react";
import { useParams } from 'react-router-dom';
import {
  Grid,
  Typography,
  Box,
} from "@mui/material";
import theme from "../theme";
import { styled, ThemeProvider } from "@mui/material/styles";
import { ContainerFormCss } from "./styles/ContainerForm.styles"

const ContainerFormTab = styled(Grid)(({ theme }) => ({
  padding: theme.spacing(4, 7),
  [`@media screen and (max-width: ${theme.breakpoints.values.md}px)`]: {
    paddingLeft: "20px",
    paddingRight: "20px",
  },
}));

const FormTab = styled(Grid)(({ theme }) => ({
  padding: theme.spacing(5),
  margin: 0,
  [`@media screen and (max-width: ${theme.breakpoints.values.md}px)`]: {
    paddingLeft: "0",
    paddingRight: "0",
  },
}));


export default function ContainerForm() {
  
  return (
    <React.Fragment>
      <ThemeProvider theme={theme}>
        <ContainerFormTab container justifyContent="center">
          <Grid
            xs={12}
            md={12}
            lg={10}
            mt={8}
            sx={ContainerFormCss.formGrid}
          >
            <FormTab xs={12} md={12} lg={12}>
              <Grid>
                  <Box>
                    <Typography color="primary" variant="h1" component="div">
                      Copiar este template para crear vistas.
                    </Typography>
                  </Box>
              </Grid>            
            </FormTab>
            
          </Grid>
        </ContainerFormTab>
      </ThemeProvider>
    </React.Fragment>
  );
}
