import React from "react"
import Cover from './cover/Cover'
import { BrowserRouter, Route, Routes } from "react-router-dom"
import Template from "./views/Template"
export default function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Cover />} />
        <Route path="/template" element={<Template />} />
      </Routes>
    </BrowserRouter>
  );
}
